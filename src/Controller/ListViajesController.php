<?php

namespace App\Controller;

use App\Entity\ListViajes;
use App\Form\ListViajesType;
use App\Repository\ListViajesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/list/viajes")
 */
class ListViajesController extends AbstractController
{
    /**
     * @Route("/", name="app_list_viajes_index", methods={"GET"})
     */
    public function index(ListViajesRepository $listViajesRepository): Response
    {
        return $this->render('list_viajes/index.html.twig', [
            'list_viajes' => $listViajesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_list_viajes_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ListViajesRepository $listViajesRepository): Response
    {
        $listViaje = new ListViajes();
        $form = $this->createForm(ListViajesType::class, $listViaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listViajesRepository->add($listViaje, true);

            return $this->redirectToRoute('app_list_viajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('list_viajes/new.html.twig', [
            'list_viaje' => $listViaje,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_list_viajes_show", methods={"GET"})
     */
    public function show(ListViajes $listViaje): Response
    {
        return $this->render('list_viajes/show.html.twig', [
            'list_viaje' => $listViaje,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_list_viajes_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ListViajes $listViaje, ListViajesRepository $listViajesRepository): Response
    {
        $form = $this->createForm(ListViajesType::class, $listViaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listViajesRepository->add($listViaje, true);

            return $this->redirectToRoute('app_list_viajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('list_viajes/edit.html.twig', [
            'list_viaje' => $listViaje,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_list_viajes_delete", methods={"POST"})
     */
    public function delete(Request $request, ListViajes $listViaje, ListViajesRepository $listViajesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$listViaje->getId(), $request->request->get('_token'))) {
            $listViajesRepository->remove($listViaje, true);
        }

        return $this->redirectToRoute('app_list_viajes_index', [], Response::HTTP_SEE_OTHER);
    }
}
