<?php

namespace App\Controller;

use App\Entity\RegistroViajes;
use App\Form\RegistroViajesType;
use App\Repository\RegistroViajesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/registro/viajes")
 */
class RegistroViajesController extends AbstractController
{
    /**
     * @Route("/", name="app_registro_viajes_index", methods={"GET"})
     */
    public function index(RegistroViajesRepository $registroViajesRepository): Response
    {
        return $this->render('registro_viajes/index.html.twig', [
            'registro_viajes' => $registroViajesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_registro_viajes_new", methods={"GET", "POST"})
     */
    public function new(Request $request, RegistroViajesRepository $registroViajesRepository): Response
    {
        $registroViaje = new RegistroViajes();
        $form = $this->createForm(RegistroViajesType::class, $registroViaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registroViajesRepository->add($registroViaje, true);

            return $this->redirectToRoute('app_registro_viajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('registro_viajes/new.html.twig', [
            'registro_viaje' => $registroViaje,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_registro_viajes_show", methods={"GET"})
     */
    public function show(RegistroViajes $registroViaje): Response
    {
        return $this->render('registro_viajes/show.html.twig', [
            'registro_viaje' => $registroViaje,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_registro_viajes_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, RegistroViajes $registroViaje, RegistroViajesRepository $registroViajesRepository): Response
    {
        $form = $this->createForm(RegistroViajesType::class, $registroViaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registroViajesRepository->add($registroViaje, true);

            return $this->redirectToRoute('app_registro_viajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('registro_viajes/edit.html.twig', [
            'registro_viaje' => $registroViaje,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_registro_viajes_delete", methods={"POST"})
     */
    public function delete(Request $request, RegistroViajes $registroViaje, RegistroViajesRepository $registroViajesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$registroViaje->getId(), $request->request->get('_token'))) {
            $registroViajesRepository->remove($registroViaje, true);
        }

        return $this->redirectToRoute('app_registro_viajes_index', [], Response::HTTP_SEE_OTHER);
    }
}
