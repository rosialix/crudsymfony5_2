<?php

namespace App\Entity;

use App\Repository\RegistroViajesRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=RegistroViajesRepository::class)
 */
class RegistroViajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Viajero::class, inversedBy="RegistroViajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Viajero;

    /**
     * @ORM\ManyToOne(targetEntity=Viajes::class, inversedBy="RegistroViajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Viajes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?Viajero
    {
        return $this->Viajero;
    }

    public function setViajero(?Viajero $Viajero): self
    {
        $this->Viajero = $Viajero;

        return $this;
    }

    public function getViajes(): ?Viajes
    {
        return $this->Viajes;
    }

    public function setViajes(?Viajes $Viajes): self
    {
        $this->Viajes = $Viajes;

        return $this;
    }
}
